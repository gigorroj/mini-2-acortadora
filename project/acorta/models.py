from django.db import models

# Create your models here.
class Contenido(models.Model):
    clave = models.CharField(max_length=256)
    valor = models.CharField(max_length=256)
    def __str__(self):
        return self.clave
