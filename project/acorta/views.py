from django.shortcuts import render

from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.template import loader
from .models import Contenido

# Create your views here.

@csrf_exempt
def index(request):
    if request.method == "POST":
        valor = request.POST['valor']
        clave = request.POST['clave']
        c = Contenido(clave=clave, valor=valor)
        c.save()
    content_list = Contenido.objects.all()
    template = loader.get_template('acorta/index.html')
    context = {
        'content_list': content_list
    }
    return HttpResponse(template.render(context, request))

@csrf_exempt
def get_content(request, llave):
    if request.method == "POST":
        valor = request.POST['valor']
        c = Contenido(clave=llave, valor=valor)
        c.save()
    try:
        contenido = Contenido.objects.get(clave=llave)
        respuesta = ("La llave " + llave + " tiene como valor: " + contenido.valor)
    except Contenido.DoesNotExist:
        respues = "NOT FOUND"
    return HttpResponse(respuesta)